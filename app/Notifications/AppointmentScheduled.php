<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Appointment;

class AppointmentScheduled extends Notification implements ShouldQueue
{
    use Queueable;


    protected $appointment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from(config('notification.sender.address'), config('notification.sender.name'))
                    ->subject(config('app.name') . ' - Interview scheduled')
                    ->line('A voice interview has been scheduled for you @ ' . $this->appointment->appointmentDate . '.')
                    ->line('With the following details: ' . $this->appointment->appointmentDescription)
                    ->line('This meeting will take place @ ' . $this->appointment->appointmentLocation . '. You will receive an email soon with details on how to join this meeting.')
                    ->line('You are expected to show up at least 5 minutes before the scheduled date.')
                    ->action('Sign in', url(route('login')))
                    ->line('Thank you!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
