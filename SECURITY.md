# Security Policy

## Supported Versions

The following versions are currently supported:

| Version | Supported          |
| ------- | ------------------ |
| 0.1.x   | :x:                |
| 0.5.x   | :x:                |
| 0.6.x   | :white_check_mark: |

## Reporting a Vulnerability

To securely report a vulnerability, you may send me an email directly containing the details of said vulnerability: ``me@nogueira.codes``.

You may optionally encrypt your message with my [public PGP key](http://pool.sks-keyservers.net/pks/lookup?op=get&search=0x48DF709E7405702B).

Use this free [online encryption tool](https://www.igolder.com/pgp/encryption/) if you don't know how to use PGP on your desktop.
