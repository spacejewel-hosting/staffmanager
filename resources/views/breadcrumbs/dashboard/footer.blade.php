<div class="dashboard-footer">

  <div class="row">

    <div class="col">

      <li class="d-inline-block">

          <a class="mr-3" href="https://github.com/spacejewel-hosting/staffmanagement"><i class="fab fa-github"></i> Github</a>
          <a class="mr-3" href="https://github.com/spacejewel-hosting/staffmanagement/issues"><i class="fas fa-bug"></i> Issue Tracker</a>

      </li>

    </div>


    <div class="col-4 d-inline-block">

      <p>&copy; Miguel N. 2020 &mdash; <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">GNU General Public License</a></p>

    </div>

  </div>

</div>
