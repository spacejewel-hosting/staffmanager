<!-- Footer -->
<footer class="page-footer font-small footer-grad">

    <!-- Copyright - DO NOT REMOVE WITHOUT PERMISSION -->
    <div class="footer-copyright text-center py-3">
        <a href="https://spacejewel-hosting.com/"> Spacejewel Hosting &copy; 2019-2020 - {{__('messages.footer_copy')}}</a>
    </div>
    <!-- Copyright -->
    <!-- Built by Miguel Nogueira -->

</footer>
<!-- Footer -->
