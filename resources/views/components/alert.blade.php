<div class="alert alert-{{$alertType}} {{$extraStyling ?? ''}}">
    <!-- Simplicity is the essence of happiness. - Cedric Bledsoe -->
    {{$slot}}
</div>
