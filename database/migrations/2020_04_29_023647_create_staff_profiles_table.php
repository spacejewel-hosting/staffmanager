<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_profiles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('userID')->unsigned();
            $table->dateTime('approvalDate');
            $table->dateTime('terminationDate')->nullable();
            $table->dateTime('resignationDate')->nullable();
            $table->text('memberNotes')->nullable();
            $table->timestamps();

            $table->foreign('userID')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_profiles');
    }
}
